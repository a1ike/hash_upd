$(document).ready(function () {
  /* $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  }); */

  $('.phone').inputmask('+7(999)999-99-99');

  $('.h-gallery__cards').slick({
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    variableWidth: true,
    focusOnSelect: true,
    centerMode: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        centerMode: false,
        variableWidth: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    }]
  });

  $('.h-top__mob-nav').on('click', function (e) {
    e.preventDefault();
    $('.h-top__nav').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.h-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.h-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.h-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.open-done').on('click', function (e) {
    e.preventDefault();
    $('.h-done').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.h-done__close').on('click', function (e) {
    e.preventDefault();
    $('.h-done').slideToggle('fast', function (e) {
      // callback
    });
  });
});
